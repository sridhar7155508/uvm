
import uvm_pkg::*;
`include "uvm_macros.svh"

class packet extends uvm_object;
  
  rand bit[7:0] addr;
  rand bit[7:0] data;
  rand bit rwb;
  
  `uvm_object_utils_begin(packet)
    `uvm_field_int(addr, UVM_NONE)
    `uvm_field_int(data, UVM_NONE)
    `uvm_field_int(rwb, UVM_NONE)
  `uvm_object_utils_end
  
  constraint c_addr{ addr > 8'h2a; };
  constraint c_data{ data inside {[8'h14:8'he9]}; };
  
  function new(string name = "packet");
    super.new(name);
  endfunction
  
endclass

class A extends uvm_component;
  `uvm_component_utils(A)
  uvm_nonblocking_put_port #(packet) pp;
  
  int num_tx;
  
  function new(string name = "A", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    pp = new("pp", this);
  endfunction
  
  virtual task run_phase(uvm_phase phase);
    phase.raise_objection(this);
    repeat(num_tx) begin
      bit success;
      packet pkt = packet::type_id::create("pkt");
      assert(pkt.randomize());
      `uvm_info("A", "packet sent to B", UVM_NONE)
      pkt.print(uvm_default_line_printer);
      
//       do begin
//         success = pp.try_put(pkt);
//         if(success)
//           `uvm_info("A", "B was ready and sucsful", UVM_NONE)
//         else
//           `uvm_info("A", "B as not ready and fail", UVM_NONE)
//         #1;
//       end while(!success);
      `uvm_info("A", "Waiting rxer to be ready",UVM_NONE)
      do begin
        success = pp.can_put();
      end while(!success);
      `uvm_info("A", "rxer is  ready", UVM_NONE)
      pp.try_put(pkt);
    end
    phase.drop_objection(this);
  endtask
  
endclass

class B extends uvm_component;
  `uvm_component_utils(B)
  uvm_nonblocking_put_imp #(packet, B) pi;
  
  function new(string name = "B", uvm_component parent);
    super.new(name, parent);
    pi = new("pi", this);
  endfunction
  
  virtual function bit try_put(packet pkt);
    //#20;
//     bit ready;
//     std::randomize(ready);
//     if(ready) begin
//       `uvm_info("B", "pacet rxd", UVM_NONE)
//       pkt.print(uvm_default_line_printer);
//       return 1;
//     end else begin
//       return 0;
//     end
    
      `uvm_info("B", "pacet rxd", UVM_NONE)
      pkt.print(uvm_default_line_printer);
      return 1;
    
  endfunction
  
//   virtual function bit can_put();
    
//   endfunction
  
  virtual function bit can_put();
    return $urandom_range(0, 1);
  endfunction
  
endclass



class test extends uvm_component;
  `uvm_component_utils(test)
  
  A A_h;
  B B_h;
  
  function new(string name = "test", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    A_h = A::type_id::create("A_h", this);
    B_h = B::type_id::create("B_h", this);
    A_h.num_tx = 2;
  endfunction
  
  virtual function void connect_phase(uvm_phase phase);
    A_h.pp.connect(B_h.pi);
  endfunction
  
endclass

module tb;
  
  initial begin
    run_test("test");
  end
  
endmodule
