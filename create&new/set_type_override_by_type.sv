`include "uvm_macros.svh"
import uvm_pkg::*;

class seq_item1 extends uvm_sequence_item;
  
  rand bit[23:0] addr;
  rand bit[23:0] data;
  rand bit wr_enb;
  
  `uvm_object_utils_begin(seq_item1)
  `uvm_field_int(addr, UVM_ALL_ON + UVM_DEC)
  `uvm_field_int(data, UVM_ALL_ON + UVM_DEC)
  `uvm_field_int(wr_enb, UVM_ALL_ON + UVM_DEC)
  `uvm_object_utils_end
  
  function new(string name = "seq_item1");
    super.new(name);
  endfunction
  
  constraint c{
    		   addr inside {[0:10]};              
               data inside {[10:40]}; 
              }
  
endclass

class seq_item2 extends seq_item1;
  `uvm_object_utils(seq_item2)
  
  function new(string name = "seq_item2");
    super.new(name);
  endfunction
 
  constraint c1{ addr == 10; }
  
endclass

uvm_factory factory = uvm_factory::get();

module top;
  
  seq_item1 seq;
  
  function void build();
    seq = seq_item1::type_id::create("seq");
    assert(seq.randomize());
    seq.print();
  endfunction
  
  initial begin
    repeat(5) begin
      `uvm_info("INFO", "Before overriding", UVM_NONE)
      build();
    end
    
    
    
    factory.set_type_override_by_type(
      								  seq_item1 :: get_type(),
                                      seq_item2 :: get_type() 
    									);
    repeat(5) begin
      `uvm_info("INFO", "Aftter overriding", UVM_NONE)
      build();
    end
  
  end
  
endmodule
