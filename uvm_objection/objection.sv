//Objection mechanism helps the simulator to decide at what time it should proceed from run_phase to extract phase

//+UVM_OBJECTION_TRACE for more details
`include "uvm_macros.svh"								//access to all uvm macros
import uvm_pkg::*;										//access to all uvm classes

////
////producer
////
class producer extends uvm_component;
  `uvm_component_utils(producer)
  
  function new(string name = "producer", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  virtual task run_phase(uvm_phase phase);
    phase.raise_objection(this);
    #10;
    `uvm_info("get_type_name()", "run phase of producer", UVM_NONE)
    phase.drop_objection(this);
  endtask
  
endclass

////
////consumer
////
class consumer extends uvm_component;
  `uvm_component_utils(consumer)
  
  function new(string name = "consumer", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  virtual task run_phase(uvm_phase phase);
    phase.raise_objection(this);
    #100;
    `uvm_info("get_type_name()", "run phase of consumer", UVM_NONE)
    phase.drop_objection(this);
  endtask
  
endclass

////
////env
////
class env extends uvm_env;
  
  `uvm_component_utils(env)
  
  producer pro;
  consumer cons;
  
  function new(string name = "", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    pro = producer::type_id::create("pro", this);
    cons = consumer::type_id::create("cons", this);
  endfunction
  
endclass

///
///test
///
class test extends uvm_test;

  `uvm_component_utils(test)
  
  env env_h;
  
  function new(string name = "", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    env_h = env::type_id::create("env_h", this);
  endfunction
  
endclass

////
//top
////
module top;
  
  initial begin
    run_test("test");
  end
endmodule
